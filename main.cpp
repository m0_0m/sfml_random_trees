#include <SFML/Graphics.hpp>
#include <vector>
#include <random>

template<typename T>
T getRand(sf::Clock& clk, const T& from, const T& to);

void ev(sf::RenderWindow& window);
void drawTrees(sf::RenderWindow& window, sf::Clock& clk, sf::RectangleShape& sh, std::vector< std::vector<int> >& myMap, unsigned N);

int main(){
    sf::RenderWindow window(sf::VideoMode(320, 640), "Trees");
    // Инициализация часов(clk), кисти(sh) и холста/карты/хз/нутыпонел(myMap)
    sf::Clock clk;
    sf::RectangleShape sh;
    sh.setFillColor(sf::Color(0,155,100,255));
    sh.setSize(sf::Vector2f(2.f, 4.f));
    sh.setPosition(0, 0);
    std::vector< std::vector<int> >myMap;

    // Главный цикл
    while (window.isOpen()){
        drawTrees(window, clk, sh, myMap, window.getSize().y);
    }

    return 0;
}

template<typename T>
T getRand(sf::Clock& clk, const T& from, const T& to){
    // Объявление и инициализация генератора случайных чисел.
    std::default_random_engine eng((unsigned int)clk.getElapsedTime().asMicroseconds());
    //clk.restart();// Если включить обновление времени, иногда получаются ёлочки.
    // Возвращение значения в пределах [from, to].
    return (T)( from + to * ((double)eng() / eng.max()) );
}

void ev(sf::RenderWindow& window){
    sf::Event event;
    while (window.pollEvent(event)){// Закрыть по нажатию на [x] или на [Escape].
        if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            window.close();
    }
}

void drawTrees(sf::RenderWindow& window, sf::Clock& clk, sf::RectangleShape& sh, std::vector< std::vector<int> >& myMap, unsigned N){
    myMap.clear();
    // Случайно заполняем первый слой по всей ширине экрана.
    for (int i = 0; i < window.getSize().x / sh.getSize().x; ++i){
        myMap.push_back(std::vector<int>());
        myMap[i].push_back(getRand<unsigned>(clk, 0, 100000) > unsigned(93000));
    }
    // Закидываем N слоёв.
    for(int k = 0; k < N; ++k){
        // Обработка событий.
        ev(window);
        // Буфер для слоя блоков.
        std::vector<int> buff;
        // Составляем план расстановки блоков из нового слоя.
        for(int i = 0; i < myMap.size(); ++i){
            if(getRand<unsigned>(clk, 0, 100000) > unsigned(93000)){// Если блок падает
                if (i == 0){// Если самая первая вертикаль проверяем под ним и справа от него.
                    // Ставим блок над самым высоким.
                    buff.push_back(std::max(myMap[i].back(), myMap[i+1].back()) + 1);
                } else if (i == myMap.size() - 1){// Если самая последняя вертикаль проверяем под ним и слева от него.
                    // Ставим блок над самым высоким.
                    buff.push_back(std::max(myMap[i].back(), myMap[i-1].back()) + 1);
                } else {// Если средняя вертикаль проверяем и слева, и справа и под ним.
                    // Ставим блок над самым высоким.
                    buff.push_back(std::max(std::max(myMap[i].back(), myMap[i-1].back()), myMap[i+1].back()) + 1);
                }
            } else {// Если блок не падает, ставим 0.
                buff.push_back(0);
            }
        }
        // Ставим слой
        for(int i = 0; i < myMap.size(); ++i){
            if(buff[i] != 0) myMap[i].push_back(buff[i]);
        }
        //Отрисовка дерева с новым слоем.
        buff.clear();
        window.clear(sf::Color(10,10,10,255));
        for(int i = 0; i < myMap.size(); ++i){
            for(int j = 0; j < myMap[i].size(); ++j){
                sh.setPosition(i * sh.getSize().x, window.getSize().y - (myMap[i][j] - 1) * sh.getSize().y);
                window.draw(sh);
            }
        }
        window.display();
    }
}
